# Zen of Python Poster

A poster made to remind you of Tim Peters' renowned “Zen of Python”. The
guiding principles of a Pythonista. The [full colour version][1] looks great
when printed on A2 paper and framed. The [black and white version][2] is
optimized for black and white printers.

| Full Colour                                                            | Printed and Framed                                                                  | Black and White                                                              |
| ---------------------------------------------------------------------- | ----------------------------------------------------------------------------------- | ---------------------------------------------------------------------------- |
| ![Alt](images/python-poster.png "Zen of Python Poster - Full Colour")  | ![Alt](images/zen-poster-framed.jpg "Zen of Python Poster - Printed and framed")    | ![Alt](images/python-poster-bw.png "Zen of Python Poster - Black and White") |


## License

Copyright © 2023 Matt Deacalion Stevens, released under the [CC BY-SA 4.0][3] License.

### You're free to:

Share
: copy and redistribute the material in any medium or format

Adapt
: remix, transform, and build upon the material for any purpose, even
  commercially.

### Under the following terms:

Attribution
: You must give appropriate credit, provide a link to the license,
  and indicate if changes were made. You may do so in any reasonable manner,
  but not in any way that suggests the licensor endorses you or your use.

ShareAlike
: If you remix, transform, or build upon the material, you must
  distribute your contributions under the same license as the original.


[1]: <../../../raw/branch/trunk/zen_poster.pdf> "Full Colour"
[2]: <../../../raw/branch/trunk/zen_poster_white_dark_grey.pdf> "Black and White"
[3]: <https://creativecommons.org/licenses/by-sa/4.0/> "CC BY-SA 4.0"
